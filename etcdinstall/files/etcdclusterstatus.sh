/opt/etcd/bin/etcdctl endpoint health \
--cacert=/opt/etcd/ssl/ca.pem \
--cert=/opt/etcd/ssl/server.pem \
--key=/opt/etcd/ssl/server-key.pem \
--endpoints="https://$1:2379,\
https://$2:2379,\
https://$3:2379"
