#!/bin/bash

cat > /opt/etcd/ssl/ca-config.json << EOF1
{
  "signing": {
    "default": {
      "expiry": "87600h"
    },
    "profiles": {
      "etcd": {
         "expiry": "87600h",
         "usages": [
            "signing",
            "key encipherment",
            "server auth",
            "client auth"
        ]
      }
    }
  }
}
EOF1

cat > /opt/etcd/ssl/ca-csr.json << EOF2
{
    "CN": "etcd CA",
    "key": {
        "algo": "rsa",
        "size": 2048
    },
    "names": [
        {
            "C": "CN",
            "L": "Beijing",
            "ST": "Beijing"
        }
    ]
}
EOF2

cfssl gencert -initca /opt/etcd/ssl/ca-csr.json | cfssljson -bare ca -
